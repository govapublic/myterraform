terraform {
    backend "s3" {
        bucket = "terraform-backend-terraformbackends3bucket-cvzkff3uawfy"
        key = "web-server"
        region = "us-east-1"
        dynamodb_table = "terraform-backend-TerraformBackendDynamoDBTable-N8ZDXYUSS1A"
    }
}