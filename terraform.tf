terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.45.0"
    }
  }
}

#default values are mentioned here like regions and tags
provider "aws" {
  region = "us-east-1"
  default_tags {
    tags ={
      Environment = "test"
      Project = "web_server"
    }
  }
}